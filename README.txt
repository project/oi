OI (Organizational Infrastructure) is a module which provides a way for
site administrators to map users to an organizational structure.  This
can then be used to restrict access to nodes.

OI allows you to define "entities", which have a hierarchical structure.
 You then add users to entities.  Entities can have defined roles, which
can be filled by one or more members.  They can also have fields (much
of the field functionality is undeveloped right now) for tracking
information like addresses, creation dates, etc.

Once OI is installed, you need to define entity types.  For example, we
have Organizations, Committees, Groups, Centers, and Hospitals.  The
purpose of the different types is to allow you to define different roles
and fields for each type; no hierarchical structure is defined by the
types.  To define types, you would go to "administer:settings:oi".  Once
you've defined the types, you can add roles (like "committee chair",
"center primary investigator", "secretary", etc) to the entity types.

Once types have been defined, you go to "administer:oi entities", where
you actually define the various entities.  The entity type can only be
set when initially creating the entity, but all other options can be
changed later.  The options here are fairly straightforward, except for
"propagate membership up the tree".  By default, when a user is added to
an entity, they are added to all the parent entities as well (this makes
sense; if you're a member of a committee in an organization, it's likely
you're a member of the organization as well).  This can be overridden on
a per-user level if there are occasional exceptions to this rule. 
However, there might be a case where members of a subcommittee shouldn't
have access (or be considered members) or the parent; in this case you
can stop members of this entity from automatically becoming members of
the parent entity.

Once entities have been set up, you can start adding users to the
various entities.  This is done via the user settings (if you have a
large number of extant users, this may take some time).  Edit the user,
and go to the OI tab (you will need to have permission to do this).  For
each entity, you have four options.
    - "" (ie, blank) means that the user is not explicitly a member
      of the entity.  However, this can be changed if a child entity
      propagates membership upwards.
    - "member" means that the user is a member of this entity,
      and potentially any parent entities.
    - "access only" means that the user is a member of the entity for
      node access purposes (ie, they can see nodes restricted to this
      entity), but not for any membership listings.
    - "*remove*" means that even if the user is a member of a child
      entity, they should not become a member of this entity (or
      any further parent entities).
Once you have set the proper options, click "submit".  At this point,
membership will be propagated up the tree, and the memberships will be
saved.  If you come back to the screen, you may see "member" next to
entities you did not set it for initially -- this is normal.

Once you have users in an entity, you can set the roles for the entity. 
To set the users for the various roles, go to "administer:oi entities",
and choose "edit data" for the entity you want to set up.  For each
role, you can edit the role.  Any role can be filled by one or more
members of the entity, but not by "access only" members.  Any role can
be shared by as many members of the committee as you like.  To remove a
user from a role, set their pop-up menu to "none".

Once you have users in entities, you can enable the restriction
mechanism.  Go to "administer:settings:oi", and click "Turn Access
Restrictions On".  (Right now, OI does not work with na_arbitrator; this
will be fixed when a version for Drupal 5 is released.)  Users who have
permission to do so will now have the option to restrict data to any of
the entities they are members of.  If you have users who need to be able
to restrict data to any entity, there is a permission that can be given
to a role to allow them to do this.

To be able to display membership lists, you need to set up the input
filter mechanism.  I'll write more about this later.
